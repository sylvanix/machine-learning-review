## Understanding Neural Networks  
... through implementing a fully-connected, multi-layer classifier  

Let's look at the basics of the "feed forward" network by considering how to update a single weight.  

```python
x = 1.9  # input data
y = 0.8 # label
w = 0.5 # initial weight
alpha = 0.1 # learning rate

for ii in range(20):
    pred = w * x
    err = (pred - y)**2
    delta_w = (pred - y) * x
    w = w - (delta_w * alpha)

    print(f"Error: {err:.4f}   Prediction: {pred:.4f}")
```



Now let's implement a multi-layer network which uses backpropagation to update the weights.  

```python
import numpy as np


np.random.seed(1)

def relu(x):
    return (x > 0) * x


def relu2deriv(x):
    return x > 0


# input data
X = np.array( [ [1, 0, 1],
                [0, 1, 1],
                [0, 0, 1],
                [1, 1, 1] ] )

# labels
y = np.array( [[1, 1, 0, 0]] ).T

alpha = 0.1 # learing rate
size_h = 4  # hidden layer size

# initialize weights
weights_0_1 = 2 * np.random.random((3, size_h)) - 1
weights_1_2 = 2 * np.random.random((size_h, 1)) - 1

n_epochs = 60
for iepoch in range(n_epochs):
    layer_2_error = 0
    for i in range(X.shape[0]):
        layer_0 = X[i:i+1]
        layer_1 = relu(layer_0 @ weights_0_1)
        layer_2 = layer_1 @ weights_1_2

        layer_2_error += np.sum((layer_2 - y[i:i+1])**2)

        layer_2_delta = layer_2 - y[i:i+1]
        layer_1_delta = (layer_2_delta @ weights_1_2.T) * relu2deriv(layer_1)

        weights_1_2 -= alpha * (layer_1.T @ layer_2_delta)
        weights_0_1 -= alpha * (layer_0.T @ layer_1_delta)

    if iepoch % 10 == 9:
        print(f"error: {layer_2_error:.6f}")
```
