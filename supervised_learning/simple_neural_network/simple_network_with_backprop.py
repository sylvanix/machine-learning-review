import numpy as np


np.random.seed(2)

def relu(x):
    return (x > 0) * x


def relu2deriv(x):
    return x > 0


# input data
X = np.array( [ [1, 0, 1],
                [0, 1, 1],
                [0, 0, 1],
                [1, 1, 1] ] )

# labels
y = np.array( [[1, 1, 0, 0]] ).T

alpha = 0.2 # learing rate
size_h = 4  # hidden layer size

# initialize weights
weights_0_1 = 2 * np.random.random((3, size_h)) - 1
weights_1_2 = 2 * np.random.random((size_h, 1)) - 1

n_epochs = 60
for iepoch in range(n_epochs):
    layer_2_error = 0
    for i in range(X.shape[0]):
        layer_0 = X[i:i+1]
        layer_1 = relu(np.dot(layer_0, weights_0_1))
        layer_2 = np.dot(layer_1, weights_1_2)

        layer_2_error += np.sum((layer_2 - y[i:i+1])**2)

        layer_2_delta = layer_2 - y[i:i+1]
        layer_1_delta = layer_2_delta.dot(weights_1_2.T) * relu2deriv(layer_1)

        weights_1_2 -= alpha * layer_1.T.dot(layer_2_delta)
        weights_0_1 -= alpha * layer_0.T.dot(layer_1_delta)

    if iepoch % 10 == 9:
        print(f"error: {layer_2_error:.6f}")

