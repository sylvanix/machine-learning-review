'''
iterative single-weight update from squred error
'''

x = 1.9  # input data
y = 0.8 # label
w = 0.5 # initial weight
alpha = 0.1 # learning rate

for ii in range(20):
    pred = w * x
    err = (pred - y)**2
    delta_w = (pred - y) * x
    w = w - (delta_w * alpha)

    print(f"Error: {err:.4f}   Prediction: {pred:.4f}")

####################################################################

