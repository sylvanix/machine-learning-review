## The Curse of Dimensionality  

The number of input variables or features for a dataset is referred to as its dimensionality. Dimensionality reduction refers to techniques that reduce the number of input variables in a dataset. More input features often make a predictive modeling task more challenging to model, more generally referred to as the curse of dimensionality.  

The performance of machine learning algorithms can degrade with too many input variables. If your data is represented using rows and columns, such as in a spreadsheet, then the input variables are the columns that are fed as input to a model to predict the target variable. Input variables are also called features. We can consider the columns of data representing dimensions on an n-dimensional feature space and the rows of data as points in that space.  

Having a large number of dimensions in the feature space can mean that the volume of that space is very large, and in turn, the points that we have in that space (rows of data) often represent a small and non-representative sample.  

___

## Bias vs. Variance  

Both overfitting and underfitting should be avoided. Recall that **bias** is
the error stemming from incorrect assumptions in the learning algorithm; high bias results in underfitting. **Variance** measures how sensitive the model prediction is to variations in the datasets. Hence, we need to avoid cases where either bias or variance is getting high. So, does it mean we should always make both bias and variance as low as possible? Yes, if we can. But there is an explicit trade-off between them, where decreasing one increases the other. This is the so-called **bias-variance trade-off**.  

---

## Logits

Logits is the unnormalized final scores of your model. You apply softmax to it to get a probability distribution over your classes.  

- Binary Classification  
Binary classification models tell whether the input belongs or not to the positive class, that is, they generate a single number between 0 and 1 representing the probability of the input belonging to the positive class, that is, they model a Bernoulli distribution conditioned on the input. Normally, the model generates an unbounded real number that is then "squashed" into the (0,1) range with the sigmoid function: σ(x) = 1 / (1 + e^-x). The unbounded real number (i.e. the unnormalized log-probability) is the logit. Note that, at inference time, in order to know if the probability is greater than 0.5, we don't need to compute the sigmoid: by the nature of the sigmoid, if the logit is greater than 0, then the probability computed with the sigmoid will be greater than 0.5.

- Multiclass Classification  
Multiclass classification models generate N probabilities, each between 0 and 1, which must sum to 1. They model a categorical distribution (sometimes referred to as "multinomial") conditioned on the input. Normally, the model generates N unbounded real numbers whose range and sum is normalized to (0,1) by means of the softmax function. The N unbounded real numbers (i.e. the unnormalized probabilities) are the logits. At inference time we do not have to calculate the softmax to know which class has the highest probability. The argmax of the logits will match the argmax after computing the softmax.

--- 

## Softmax

The softmax ("softargmax") function, or _normalized exponential function_, converts a vector of k numbers into a probability distribution of k outcomes. This function is often used as the last activation function of an artificial neural network since it normalizes the output of a network to a probability distribution over predicted output classes.  

Example, apply softmax to a vector a:  

```python
>>> import numpy as np
>>> a = [1.0,2.0,3.0,4.0,3.0,2.0,1.0]
>>> np.exp(a) / sum(np.exp(a))
array([0.02364054, 0.06426166, 0.1746813, 0.474833, 0.1746813, 0.06426166, 0.02364054])

# note that the softmax output must sum to 1
>>> np.sum( np.exp(a) / sum(np.exp(a)) )
0.9999999999999999
```

Why use softmax as opposed to standard normalization?  

The exp() in the softmax function roughly cancels out the log in the cross-entropy loss causing the loss to be roughly linear. This leads to a roughly constant gradient, when the model is wrong, allowing it to correct itself quickly. Thus, a wrong saturated softmax does not cause a vanishing gradient.

---

## Loss Functions  

**L1 and L2 loss**  
L1 and L2 are loss functions used in regression machine learning models.
These functions are applied to each observation in a dataset to measure how accurate a given prediction is in comparison to the actual value. The resulting loss values can then be aggregated to a dataset level where they would give an indication of the model accuracy over the whole dataset. This aggregation is called the cost function.  
L1, also known as the Absolute Error Loss, is the absolute difference between the prediction and the actual.  
L2, also known as the Squared Error Loss, is the squared difference between the prediction and the actual.  
	
It might help to see them implemented in Python:  

```
import numpy as np

actual     = np.array([10, 11, 12, 13])
prediction = np.array([10, 12, 14, 17])

l1_loss = abs(actual - prediction)
# Output: array([0, 1, 2, 4])

l2_loss = (actual - prediction) ** 2
# Output: array([ 0,  1,  4, 16])
```

The difference between the two losses is very evident when we look at the outlier in the dataset. The L2 loss for this observation is considerably larger relative to the other observations than it was with the L1 loss. This is the key differentiator between the two loss functions.  

**MAE**  
MAE (Mean Absolute Error, sometimes called the L1 loss), is the average absolute error between actual and predicted values.

MAE is a popular metric to use as the error value is easily interpreted, as the value is in the same scale as the target you are predicting for.

MAE = sum( (actual - prediction) ) / N

where N is the number of observations.

**MSE**   
MSE (Mean Squared Error, sometimes called the L2 loss) is the average squared error between actual and predicted values.

The main draw for using MSE is that it squares the error, which results in large errors being punished or clearly highlighted, and is therefore useful when working on models where occasional large errors must be minimized.

MSE = sum( (actual - prediction)^2 ) / N

where N is the number of observations.    

**Cross Entropy**  
Cross-entropy (also known as negative log likelihood loss, NLL), is a commonly used loss function in machine learning for classification problems. The function measures the difference between the predicted probability distribution and the true distribution of the target variables. It is commonly used in supervised learning problems with multiple classes, such as in a neural network with softmax activation. The cross-entropy loss is used as the optimization objective during training to adjust the model’s parameters.  

---

## Optimization Algorithms  

- SGD


- AdaGrad
- RMSProp
- Adam

---

The next three topics are methods for avoiding overfitting:

* Cross-validation
* Regularization
* feature selection & dimensionality reduction

## Cross-Validation  

Using conventional validation, the original data is partitioned into three subsets, say 60% for the training set, 20% for the validation set, and the remaining 20% for the testing set. If the data set is small, however, cross-validation is preferable. In one round of cross-validation, the original data is divided into two subsets, for training and testing (or validation), respectively. The testing performance is recorded. Similarly, multiple rounds of cross-validation are performed under different partitions. Testing results from all rounds are finally averaged to generate a more reliable estimate of model prediction performance. Cross-validation helps to reduce variability and, therefore, limit overfitting.

The two most common cross-validation schemes are _exhaustive_ and _non-exhaustive_. In the exhaustive scheme, we leave out a fixed number of observations in each round as testing (or validation) samples and use the remaining observations as training samples. This process is repeated until all possible different subsets of samples are used for testing once. For instance, we can apply Leave-One-Out-Cross-Validation (LOOCV), which lets each sample be in the testing set once. For a dataset of the size n, LOOCV requires n rounds of cross-validation. This can be slow when n gets large.  

In the non-exhaustive scheme, on the other hand, all possible partitions are not employed. The most widely used variation is k-fold cross-validation. We first randomly split the original data into k equal-sized folds. In each trial, one of these folds becomes the testing set, and the rest of the data becomes the training set. This process is repeated k times, with each fold being the designated testing set once. Finally, we average the k sets of test results for the purpose of evaluation. Common values for k are 3, 5, and 10.  

K-fold cross-validation often has a lower variance compared to LOOCV, since a chunk of samples is used instead of a single one for validation.  
									
## Regularization  

**Early Stopping**  
xxxx

**Bagging and other ensemble methods**  
xxx

**Dropout**  
xxx

## Gradient Descent  

GD is an optimization algorithm that minimizes a target function. It does this by iteratively updating the input parameters of the function, using the gradient to guide the updating.  

**Relation of backpropagation to gradient descent**  
SGD updates the parameters of a predictive model to minimize a loss function. It does that by using the gradients of the loss function with respect to the parameters. BP provides one way to calculate those gradients.  

**GD and SGD**  
n both gradient descent (GD) and stochastic gradient descent (SGD), you update a set of parameters in an iterative manner to minimize an error function.

While in GD, you have to run through ALL the samples in your training set to do a single update for a parameter in a particular iteration, in SGD, on the other hand, you use ONLY ONE or SUBSET of training sample from your training set to do the update for a parameter in a particular iteration. If you use SUBSET, it is called Minibatch Stochastic gradient Descent.

Thus, if the number of training samples are large, in fact very large, then using gradient descent may take too long because in every iteration when you are updating the values of the parameters, you are running through the complete training set. On the other hand, using SGD will be faster because you use only one training sample and it starts improving itself right away from the first sample.

SGD often converges much faster compared to GD but the error function is not as well minimized as in the case of GD. Often in most cases, the close approximation that you get in SGD for the parameter values are enough because they reach the optimal values and keep oscillating there.

The inclusion of the word stochastic simply means the random samples from the training data are chosen in each run to update parameter during optimisation, within the framework of gradient descent.  
