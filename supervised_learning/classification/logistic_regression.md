### Logistic Regression  

Logistic regression is a widely-used classification model that is easy to implement and performs very well on linearly separable classes. Note that logistic regression can be readily generalized to multiclass settings, which is known as multinomial logistic regression, or softmax regression.  

Similar to linear regression, logistic regression is also used to estimate the relationship between a dependent variable and one or more independent variables, but it is used to make a prediction about a categorical variable versus a continuous one.  

Let the variable p represent the odds in favor of a certain event (a patient having a given disease, for example), then we can define the probability p as

p := p(y = 1|x),

the conditional probability that a particular example belongs to a certain class 1 given its features, x.

the logit function is simply the logarithm of the odds (log-odds):  

logit(p) = log( p / (1 - p) )  

Note that log refers to the natural logarithm, as it is the common convention in computer science.  The logit function takes input values in the range 0 to 1 and transforms them into values over the entire real-number range. Under the logistic model, we assume that there is a linear relationship between the weighted inputs and the log-odds:  

logit(p) = w1\*x1 + w2\*x2 + ... + wn\*xn + b  

While the preceding describes an assumption we make about the linear relationship between the log-odds and the net inputs, what we are actually interested in is the probability p, the class-membership probability of an example given its features. While the logit function maps the probability to a real-number range, we can consider the inverse of this function to map the real-number range back to a [0, 1] range for the probability p.  

This inverse of the logit function is typically called the logistic sigmoid function, which is sometimes simply abbreviated to sigmoid function due to its characteristic S-shape:  

𝜎(𝑧) = 1 / (1 + exp^(-z))  

The plot of 𝜎(𝑧) has a distictive "s-like" shape:  

<img src="sigmoidal_curve.png" width="400" />  

𝜎(𝑧) approaches 1 if z goes toward infinity (z→∞) since e^(–z) becomes very small for large values of z. 𝜎(𝑧) goes toward 0 for z→–∞ as a result of an increasingly large denominator. The sigmoid function takes real-number inputs and transforms them into the range [0,1].   

The output of the sigmoid function is then interpreted as the probability of a particular example belonging to a class. There are many applications where we are not only interested in the predicted class labels, but where the estimation of the class-membership probability is particularly useful (the output of the sigmoid function prior to applying the threshold function).  

