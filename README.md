# Machine Learning Review  

Examples of applied ML and a Review of Deep Learning

## Supervised Learning

### Regression  

- Iteratively fitting a straight line in NumPy  
	[Learning to find the slope and intercept](./supervised_learning/regression/learning_to_fit_a_line.ipynb)  
	
- Training with Gradient Descent - NumPy    
	[Life Expectancy regression with GD](./supervised_learning/regression/regression_with_GD.ipynb)  	

- Xtreme Gradient Boosting for Regression  
	[Kaggle "Crab Age" prediction](./supervised_learning/regression/crab_age_prediction.ipynb)  
	
- Neural Network Regression Model with PyTorch  
	[House Price prediction](./supervised_learning/regression/ames\_housing\_regression.ipynb)  

### Classification  

- A Logistic Regression Binary Classifier in NumPy  
	[an example from a great introductory book](./supervised_learning/classification/logistic_regression_in_NumPy.ipynb)   

- Exploratory Data Analysis and Logistic Regression Classification   
	[heart disease prediction](./supervised_learning/classification/heart_disease_prediction.ipynb)  
	
	[An explanation of Logistic Regression](./supervised_learning/classification/logistic_regression.md)  

- A Sampling of Scikit-Learn Classifiers (Random Forest, XGBoost, Decision Tree, Navie Bayes, Logistic Regression, K-Nearest Neighbors)    
	[asteroids classification](./supervised_learning/classification/asteroids_classification.ipynb)  
	
- Image classification  
	[ResNet18 for ImageNet images (imagenette)](./supervised_learning/classification/train_ResNet-18_classifier_imagenette.ipynb)  
	
### Anomaly Detection  
- Autoencoder for anomaly detection  
	[credit card fraud detection](./supervised_learning/autoencoder/credit_card_fraud.ipynb)  

	


## Unsupervised Learning  
... in progress ...


### Large Language Models (LLMs)  

- [Using open\_llama\_3b\_v2 and Hugging Face Transformers](./LLMs/using_open_llama_v2.ipynb)  


## Review of Deep Learning  

- [Deep Learning concepts and glossary](./supervised_learning/deep_learning/dl.md)   

- [A NumPy artificial neural network implementation](./supervised_learning/simple_neural_network/simple_nn.md)  

- [Create and use a custom dataset class in PyTorch](./supervised_learning/classification/pytorch_mnist.ipynb)   

- [Load mnist from file and train with TensorFlow](./supervised_learning/classification/tf_mnist.ipynb)   
 
